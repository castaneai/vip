import Foundation
import AVKit

@main
public struct VideoImageProcessor {
    public private(set) var text = "Hello, World!"

    public static func main() {
        guard let path = CommandLine.arguments.dropFirst().first else {
            log("arg(video file path) required")
            return
        }
        
        let asset = AVAsset(url: URL(filePath: path))
        let gen = AVAssetImageGenerator(asset: asset)
        let timeRange = CMTimeRange(start: .zero, duration: CMTime(seconds: 5.0, preferredTimescale: 1))
        let times = generateTimes(asset: asset, timeRange: timeRange, fps: 15).map { NSValue(time: $0) }
        
        var f = 0
        gen.generateCGImagesAsynchronously(forTimes: times) { requestedTime, image, _, result, error in
            debugPrint("f: \(f), t: \(requestedTime.seconds)")
            if result != .succeeded {
                debugPrint("error occured: \(String(describing: error))")
                return
            }
            guard let image = image else { return }
            let destFileName = String(format: "out_%d.png", f)
            let dest = CGImageDestinationCreateWithURL(URL(filePath: destFileName) as CFURL, UTType.png.identifier as CFString, 1, nil)
            CGImageDestinationAddImage(dest!, image, nil)
            CGImageDestinationFinalize(dest!)
            f += 1
            if f >= times.count {
                debugPrint("finish")
            }
        }
        sleep(5000)
    }
}

func generateTimes(asset: AVAsset, timeRange: CMTimeRange, fps: Int) -> [CMTime] {
    let frameDuration = CMTimeMake(value: 1, timescale: Int32(fps))
    var time: CMTime = timeRange.start
    var requestedTimes: [CMTime] = []
    while CMTimeCompare(time, timeRange.end) <= 0 {
        requestedTimes.append(time)
        time = CMTimeAdd(time, frameDuration)
    }
    return requestedTimes
}
